import os, sys

from typing import Iterable as Iter
from typing import List

def extend_import_paths(directory_paths: List[str]):
    # Input can be relative. "../tests" etc.
    
    assert(type(directory_paths) == list) # must be list of str
    
    isdir = os.path.isdir
    abspath = os.path.abspath
    
    abs_paths = map(lambda x: abspath(x), directory_paths)
        
    # Find new paths using sets:
    sys_paths: Set[str] = set(sys.path)
    abs_set: Set[str] = set(abs_paths)
    
    news = abs_set.difference(sys_paths)
    
    for p in news:
        if isdir(p):
            sys.path.append(p)
        #
    #
    #sys.path.extend(list(news))  # Update import search locations.
    
    print("New paths: ", '\n'.join(list(news)))
    print("<<<<<<< END New paths")
    pass
#

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
#sys.path.append(r"D:\Documents\temp\ExamplePythonProject\source\Project")

paths_to_add = ["..", "../Project", "../Project", "..\Project\SubDir1\SubDir2" \
            , "..\Project\SubDir1", "19"
]

extend_import_paths( paths_to_add )



#print('\n'.join(sys.path))

import class1
import class2
import class3



print(">>>>>>>>>>>>> sys.path:")

print('\n'.join(sys.path))

print("***** END sys.path *****")

print("test1 end")
"""
import importlib
mod =importlib.reload("class1")
"""